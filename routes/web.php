<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('auth.login');
});

Route::get('/services',function(){
	return view('services');
});

Route::get('/portfolio',function(){
	return view('portfolio');
});

Route::get('/contact/us', 'Mycontroller@index')->name('contact');

Route::get('/about',function(){
	return view('about', ['project' => 'Sheikh Kamal It']);
});

/* Data store */

Route::post('/storess', 'Mycontroller@store')->name('store');

/* registation form */

Route::get('/registation', function(){
	return view('registation');
})->name('registation')->middleware('verified');
//Data store
Route::post('/reg/store', 'Mycontroller@RegStore')->name('reg/store');
//All data view
Route::get('lamia/index', 'Mycontroller@lamia')->name('lamia/index')->middleware('verified');
//single data view
Route::get('single/view/{id}', 'Mycontroller@show');
//delete
Route::get('single/delete/{id}', 'Mycontroller@destroy');
//edit
Route::get('/edit/{id}', 'Mycontroller@edit');
//Update
Route::post('/update/{id}', 'Mycontroller@update');

//images upload
Route::get('/image', 'Mycontroller@image')->middleware('verified');
Route::post('image/store','Mycontroller@imagestore')->name('image.store');
Route::get('/image/all', 'Mycontroller@ImagleAll')->name('alluser')->middleware('verified');
Route::get('/image/single/{id}', 'Mycontroller@ImageSingle')->name('image.single');

Route::get('/image/delete/{id}', 'Mycontroller@ImageDelete');
Route::get('/image/edit/{id}', 'Mycontroller@ImageEdit');

Route::post('/image/update/{id}', 'Mycontroller@ImageUpdate');



Auth::routes(['verify' => true]);

Route::get('/home', 'HomeController@index')->name('home')->middleware('verified');

Route::get('/pdf', 'Mycontroller@pdf');
Route::get('/excel/export', 'Mycontroller@excel')->name('excel.export');
