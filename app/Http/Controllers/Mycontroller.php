<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use PDF;
use App\Exports\UsersExport;
use Maatwebsite\Excel\Facades\Excel;

class Mycontroller extends Controller
{
    public function index(){
        
       // DB::table('students')->get();

        //return view('contact');

    }
    
    public function RegStore(Request $request){
          $validatedData = $request->validate([
            'name' => 'required|max:255|min:3',
            'email' => 'required|unique:registations',
            'mobile' => 'required|unique:registations',
        ]);

        $data=array();
        $data['name']=$request->name;
        $data['email']=$request->email;
        $data['mobile']=$request->mobile;
        // print_r($data);
        // die();
       $query= DB::table('registations')->insert($data);
       if ($query) {
         $notification = array(
                'message' => 'Data insert successful!', 
                'alert-type' => 'success'
            );

            return redirect()->route('lamia/index')->with($notification);
       }
       else{
      $notification = array(
                'message' => 'insert failed!', 
                'alert-type' => 'error'
            );

            return redirect()->back('')->with($notification);
       }
    }

    public function lamia(){
        $data=DB::table('registations')->get();
        return view('alldata', compact('data'));
    }

    public function show($id){
        $data=DB::table('registations')->where('id',$id)->first();
        
        return view('view', compact('data'));
    }

    public function destroy($id){
        $data=DB::table('registations')->where('id',$id)->delete();
        
        if ($data) {
         $notification = array(
                'message' => 'Data destroy successful!', 
                'alert-type' => 'success'
            );

            return redirect()->route('lamia/index')->with($notification);
       }
       else{
      $notification = array(
                'message' => 'destroy failed!', 
                'alert-type' => 'error'
            );

            return redirect()->route('lamia/index')->with($notification);
       }
    }

    public function edit($id){
     $data= DB::table('registations')->where('id', $id)->first();
      return view('edit', compact('data'));
    }

    public function update(Request $request, $id){
      $validatedData = $request->validate([
            'name' => 'required|max:255|min:3',
            'email' => 'required',
            'mobile' => 'required',
        ]);

        $data=array();
        $data['name']=$request->name;
        $data['email']=$request->email;
        $data['mobile']=$request->mobile;
        // print_r($data);
        // die();
       $query= DB::table('registations')->where('id',$id)->update($data);
       if ($query) {
         $notification = array(
                'message' => 'Data Update successful!', 
                'alert-type' => 'success'
            );

            return redirect()->route('lamia/index')->with($notification);
       }
       else{
      $notification = array(
                'message' => 'Nothing to update', 
                'alert-type' => 'error'
            );

            return redirect()->route('lamia/index')->with($notification);
       }

    }

    public function image(){

     $data=DB::table('registations')->get();
        return view('image', compact('data'));
    }

    public function imagestore(Request $request){
      $validatedData = $request->validate([
            'reg_id' => 'required',
            'images' => 'required | mimes:jpeg,jpg,png,gif | max:2000',
            
        ]);

      $data=array();
      $data['reg_id']=$request->reg_id;

      $image=$request->file('images');
      $image_name=hexdec(uniqid());
      $ext=strtolower($image->getClientOriginalExtension());
      $new_image=$image_name.'.'. $ext;
      $path='public/images/';
      $img_url=$path. $new_image;

      $success=$image->move($path,$new_image);

      $data['images']=$img_url;

       $query= DB::table('students')->insert($data);
       if ($query) {
         $notification = array(
                'message' => 'Data insert successful!', 
                'alert-type' => 'success'
            );

            return redirect()->route('alluser')->with($notification);
       }
       else{
      $notification = array(
                'message' => 'insert failed!', 
                'alert-type' => 'error'
            );

            return redirect()->back('')->with($notification);
       }

    }

    public function ImagleAll(){
      //$lamia=DB::table('students')->get();
      $lamia=DB::table('students')->join('registations','students.reg_id','registations.id' )->select('students.*', 'registations.name')->get();




      return view('alluser', compact('lamia'));
    }


    public function ImageSingle($id){

       $data=DB::table('students')->join('registations','students.reg_id','registations.id' )->select('students.*', 'registations.name')->where('students.id', $id)->first();
       return view('singleimage', compact('data'));

    }

    public function ImageDelete($id){
      $image=DB::table('students')->where('id',$id)->first();
      $imgpath=$image->images;
      unlink($imgpath);
       $data=DB::table('students')->where('id',$id)->delete();
        
        if ($data) {
         $notification = array(
                'message' => 'Data destroy successful!', 
                'alert-type' => 'success'
            );

            return redirect()->route('alluser')->with($notification);
       }
       else{
      $notification = array(
                'message' => 'destroy failed!', 
                'alert-type' => 'error'
            );

            return redirect()->route('alluser')->with($notification);
       }
    }

    public function ImageEdit($id){

      $registations=DB::table('registations')->get();

      $students= DB::table('students')->where('id', $id)->first();
      return view('editimage', compact('registations', 'students'));
    }

    public function ImageUpdate(Request $request, $id){

      $validatedData = $request->validate([
            'reg_id' => 'required',
            'images' => 'mimes:jpeg,jpg,png,gif | max:2000',
            
        ]);

      $data=array();
      $data['reg_id']=$request->reg_id;
      $image=$request->file('lamia');
      
       if ($image) {

      $image_name=hexdec(uniqid());
      $ext=strtolower($image->getClientOriginalExtension());
      $new_image=$image_name.'.'. $ext;
      $path='public/images/';
      $img_url=$path. $new_image;

      $success=$image->move($path,$new_image);
      $old_path=$request->old_image;
      unlink($old_path);

      $data['images']=$img_url;
       $query= DB::table('students')->where('id', $id)->update($data);


         $notification = array(
                'message' => 'Data update successful!', 
                'alert-type' => 'success'
            );

            return redirect()->route('alluser')->with($notification);
       }
       else{

         $data['images']=$request->old_image;
             DB::table('students')->where('id',$id)->update($data);
           
           $notification = array(
                'message' => 'Not yet change', 
                'alert-type' => 'error'
            );

            return redirect()->route('alluser')->with($notification);
       }

    }


    public function pdf()
        {
            $data=DB::table('registations')->get();
            $pdf = PDF::loadView('myPDF', compact('data'));
      
            return $pdf->download('myPDF.pdf');
        }
        public function excel(){
          return Excel::download(new UsersExport, 'users.xlsx');
        }
    

}

