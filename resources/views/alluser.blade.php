@extends('layout')

@section('content')

	<section class="page-section" id="contact">
    <div class="container">
      <div class="row">
        <div class="col-lg-12 text-center">
          <h2 class="section-heading text-uppercase">Registation Form</h2>
          
        </div>
      </div>
      <div class="row">
        <div class="col-lg-12">
          
        	
        	<table class="table table-striped table-light" id="myTable">
			  <thead class="thead-dark">
			    <tr>
			      <th scope="col">ID</th>
			      <th scope="col">reg_id</th>
			      <th scope="col">images</th>
			      
			      <th scope="col">Create Date</th>
			      <th scope="col">Action</th>
			    </tr>
			  </thead>
			  <tbody>
			    @foreach($lamia as $sohid)
			    <tr>
			      <th scope="row">{{$sohid->id}}</th>
			      <td>{{$sohid->name}}</td>
			      <td><img src="{{url($sohid->images)}}" style="height:200px; width: 200px "></td>
			     
			      <td>{{$sohid->created_at}}</td>
			      <td>

			      	<a class="btn btn-info" href="{{url('image/single/'.$sohid->id)}}" role="button">View</a>


			      	<a class="btn btn-warning" href="{{url('image/edit/'.$sohid->id)}}" role="button">Edit</a>
			      	<a class="btn btn-danger" href="{{url('/image/delete/'.$sohid->id)}}" role="button">Delete</a>

			      </td>
			    </tr>
			    @endforeach
			    
			  </tbody>
			</table>
          
        </div>
      </div>
    </div>
  </section>
@endsection