@extends('layouts.app')

	<section class="page-section" id="contact">
    <div class="container">
      <div class="row">
        <div class="col-lg-12 text-center">
          <h2 class="section-heading text-uppercase">User data</h2>
          
        </div>
      </div>
      <div class="row">
        <div class="col-lg-12">
          
        	
        	<table class="table table-striped table-light" id="myTable">
			  <thead class="thead-dark">
			    <tr>
			      <th scope="col">ID</th>
			      <th scope="col">Name</th>
			      <th scope="col">Email</th>
			      <th scope="col">Mobile</th>
			      <th scope="col">Create Date</th>
			   
			    </tr>
			  </thead>
			  <tbody>
			    @foreach($data as $sohid)
			    <tr>
			      <th scope="row">{{$sohid->id}}</th>
			      <td>{{$sohid->name}}</td>
			      <td>{{$sohid->email}}</td>
			      <td>{{$sohid->mobile}}</td>
			      <td>{{$sohid->created_at}}</td>
			     
			    </tr>
			    @endforeach
			    
			  </tbody>
			</table>
          
        </div>
      </div>
    </div>
  </section>
