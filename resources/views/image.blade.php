@extends('layout')

@section('content')

	<section class="page-section" id="about">
    <div class="container">
      <div class="row">
        <div class="col-lg-12 text-center">
          <h2 class="section-heading text-uppercase">Registation Form</h2>
          
        </div>
      </div>
      <div class="row">
        <div class="col-lg-12">
          
        	@if ($errors->any())
			    <div class="alert alert-danger">
			        <ul>
			            @foreach ($errors->all() as $error)
			                <li>{{ $error }}</li>
			            @endforeach
			        </ul>
			    </div>
			@endif
<!-- Create Post Form -->
        <form action="{{route('image.store')}}" enctype="multipart/form-data" method="post">
          @csrf
            
            <div class="form-group">
              <label for="name">Name</label>
               <select name="reg_id" class="form-control" id="exampleFormControlSelect1">
				     @foreach($data as $row)
				      <option value="{{$row->id}}">{{$row->name}}</option>
				     @endforeach
				</select>
              
            </div>
            <div class="form-group">
              <label for="exampleFormControlFile1">Profile picture</label>
   				 <input type="file" name="images" class="form-control-file" id="exampleFormControlFile1">
              
            </div>
            
  
 			 <button type="submit" class="btn btn-primary">Submit</button>
		</form>
        </div>
      </div>
    </div>
  </section>
@endsection