@extends('layout')

@section('content')

	<section class="page-section" id="contact">
    <div class="container">
      <div class="row">
        <div class="col-lg-12 text-center">
          <h2 class="section-heading text-uppercase">Hello Mr. {{$data->name}}</h2>
          <p style="background: #ddd; color: red">Please check your information</p>
          
        </div>
      </div>
      <div class="row">
        <div class="col-lg-12">
          
        	@if ($errors->any())
			    <div class="alert alert-danger">
			        <ul>
			            @foreach ($errors->all() as $error)
			                <li>{{ $error }}</li>
			            @endforeach
			        </ul>
			    </div>
			@endif
<!-- Create Post Form -->
          <form action="{{url('update/'.$data->id)}}" method="post">
          @csrf
            
            <div class="form-group">
              <label for="name">Name</label>
              <input type="text" class="form-control" id="name" name="name"  value="{{$data->name}}">
              
            </div>
            <div class="form-group">
              <label for="exampleInputEmail1">Email address</label>
              <input type="email" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" name="email" value="{{$data->email}}">
              
            </div>
            <div class="form-group">
              <label for="mobile">Mobile</label>
              <input type="number" class="form-control" id="mobile" aria-describedby="emailHelp" name="mobile" value="{{$data->mobile}}">
              
            </div>
  
  <button type="submit" class="btn btn-primary">Submit</button>
</form>
        </div>
      </div>
    </div>
  </section>
@endsection