@extends('layouts.app')

@section('content')

	<section class="page-section" id="contact">
    <div class="container">
      <div class="row">
        <div class="col-lg-12 text-center">
          <h2 class="section-heading text-uppercase">Registation Form</h2>
          <a class="btn btn-primary" href="{{url('pdf')}}" role="button">Download PDF</a>
          <a class="btn btn-primary" href="{{Route('excel.export')}}" role="button">Download Excel</a>

          
        </div>
      </div>
      <div class="row">
        <div class="col-lg-12">
          
        	
        	<table class="table table-striped table-light" id="myTable">
			  <thead class="thead-dark">
			    <tr>
			      <th scope="col">ID</th>
			      <th scope="col">Name</th>
			      <th scope="col">Email</th>
			      <th scope="col">Mobile</th>
			      <th scope="col">Create Date</th>
			      <th scope="col">Action</th>
			    </tr>
			  </thead>
			  <tbody>
			    @foreach($data as $sohid)
			    <tr>
			      <th scope="row">{{$sohid->id}}</th>
			      <td>{{$sohid->name}}</td>
			      <td>{{$sohid->email}}</td>
			      <td>{{$sohid->mobile}}</td>
			      <td>{{$sohid->created_at}}</td>
			      <td>
			      	<a class="btn btn-info" href="{{url('single/view/'.$sohid->id)}}" role="button">View</a>
			      	<a class="btn btn-warning" href="{{url('edit/'.$sohid->id)}}" role="button">Edit</a>
			      	<a class="btn btn-danger" href="{{url('single/delete/'.$sohid->id)}}" role="button">Delete</a>

			      </td>
			    </tr>
			    @endforeach
			    
			  </tbody>
			</table>
          
        </div>
      </div>
    </div>
  </section>
@endsection