@extends('layouts.app')

@section('content')
 <div id="registation">
<div class="container">
    <div id="registation-row" class="row justify-content-center align-items-center">
                <div id="registation-column" class="col-md-6">
                    <div id="registation-box" class="col-md-12">
                        <form id="registation-form" class="form" method="POST" action="{{ route('register') }}">
                            @csrf

                            <h3 class="text-center text-info">Registation</h3>
                            <div class="form-group">
                                <label for="username" class="text-info">Name:</label><br>
                                
                                <input type="text" id="username" class="form-control @error('name') is-invalid @enderror "  name="name" value="{{ old('name') }}" required autocomplete="name" autofocus>

                                  @error('name')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                                  
                            </div>

                            <div class="form-group">
                                <label for="username" class="text-info">E-Mail Address:</label><br>
                                <input type="email"  id="username" class="form-control @error('email') is-invalid @enderror"  name="email" value="{{ old('email') }}" required autocomplete="email">

                                 @error('email')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                                  
                            </div>

                            <div class="form-group">
                                <label for="password" class="text-info">Password:</label><br>
                                <input type="password" name="password" id="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="new-password">

                                 @error('password')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>

                            <div class="form-group">
                                <label for="password" class="text-info">Confirm Password:</label><br>
                                <input class="form-control" type="password" id="password"name="password_confirmation" required autocomplete="new-password">

                                
                            </div>

                            <div class="form-group">
                                
                                <input type="submit" name="submit" class="btn btn-info btn-md" value="submit">
                            </div>
                            
                            <div id="register-link" class="text-right">
                                <a href="{{ route('login') }}" class="text-info">Login here</a>
                                 
                            </div>
                        </form>
                    </div>
                </div>
            </div>
</div>
</div>
@endsection
