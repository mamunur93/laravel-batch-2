@extends('layout')

@section('content')

	<section class="page-section" id="Portfolio" style="background:  #ddd;">
    <div class="container">
      <div class="row">
        <div class="col-lg-12 text-center">
          <h2 class="section-heading text-uppercase">Registation Form</h2>
          
        </div>
      </div>
      <div class="row">
        <div class="col-lg-12">
          
        	@if ($errors->any())
			    <div class="alert alert-danger">
			        <ul>
			            @foreach ($errors->all() as $error)
			                <li>{{ $error }}</li>
			            @endforeach
			        </ul>
			    </div>
			@endif
<!-- Create Post Form -->
        <form action="{{url('/image/update/'.$students->id)}}" enctype="multipart/form-data" method="post">
          @csrf
            
            <div class="form-group">
              <label for="name">Name</label>
               <select name="reg_id" class="form-control" id="exampleFormControlSelect1">
				     @foreach($registations as $row)
				      <option value="{{$row->id}}" @if ($row->id == $students->reg_id) {{ 'Selected' }} @endif>{{$row->name}} </option>
				     @endforeach
				</select>
              
            </div>
            <div class="form-group">
              <label for="exampleFormControlFile1">Profile picture</label>
   				 <input type="file" name="lamia" class="form-control-file" id="exampleFormControlFile1">
             
              <label for="exampleFormControlFile1">Profile picture</label>
              <img src="{{url($students->images)}}" style="width: 150px; height: 150px">
             
              <input type="hidden" value="{{$students->images}}" name="old_image">
            </div>
            
  
 			 <button type="submit" class="btn btn-primary">Update</button>
		</form>
        </div>
      </div>
    </div>
  </section>
@endsection