@extends('layout')

@section('content')

	<section class="page-section" id="contact">
    <div class="container">
      <div class="row">
        <div class="col-lg-12 text-center">
          <h2 style="background: #ddd;color:red" class="section-heading text-uppercase text-center">Hello Mr/Mrs/Miss {{$data->name}}</h2>
          <a class="btn btn-info" href="{{route('lamia/index')}}" role="button" style="float: right; margin-bottom: 5px ">Go Back</a>
        </div>
      </div>
      <div class="row">
        <div class="col-lg-12">
          
        	
        	<table class="table table-striped table-light">
			  
			  <tbody>

			    <tr>
			      <th scope="row">{{$data->id}}</th>
			      <td>{{$data->name}}</td>
			      <td>{{$data->email}}</td>
			      <td>{{$data->mobile}}</td>
			      <td>{{$data->created_at}}</td>
			     
			    </tr>
			   
			    
			  </tbody>
			</table>
          
        </div>
      </div>
    </div>
  </section>
@endsection